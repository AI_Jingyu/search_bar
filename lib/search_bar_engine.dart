import 'package:flutter/material.dart';
import 'asset.dart';

class SearchBar extends StatefulWidget {
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SearchBar'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: (){
              // print('click search to verify the button works');
              showSearch(context: context, delegate: SearchBarImplementaion());
            },
          ),
        ],
      ),
    );
  }
}

class SearchBarImplementaion extends SearchDelegate<String> {

  //icon X, on the right part of bar
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: ()=>query="",
      ),
    ];
  }

  //icon <-, on the left part of bar
  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
        icon: AnimatedIcons.menu_arrow, 
        progress: transitionAnimation,
      ),
      onPressed: ()=>close(context, null),
    );
  }

  //result after click seach button
  @override
  Widget buildResults(BuildContext context) {
    return Container(
      width: 100.0,
      height: 100.0,
      child: Card(
        color: Colors.redAccent,
        child: Center(
          child: Text(query),
        ),
      ),
    );
  }

  //metaphor to tell the function to customer
  @override
  Widget buildSuggestions(BuildContext context) {
    final suggestionList=query.isEmpty
      ?recommend
      :searchList.where((input)=>input.startsWith(query)).toList();

    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: (context,index)=>ListTile(
        title: RichText(
          //make input text bold
          text: TextSpan(
            text: suggestionList[index].substring(0,query.length),
            style: TextStyle(
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),
            children: [
              TextSpan(
                text: suggestionList[index].substring(query.length),
                style: TextStyle(color: Colors.grey)
              ),
            ],
          ),
        ),
      ),
    );
  }
}